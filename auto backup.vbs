'
'
' Sn0wlink Auto Backup
' 
' ====================================
' Ver 1.2
' Author: David Collins-Cubitt
' Company: Sn0wlink IT
' Application: To auto backup a folder and rename by date
' Date: Jan 2016
' ====================================
' ====================================

' Message box to copy files
MsgBox "Auto Backup - ready to copy!", vbOKOnly, "Auto Backup"

' ====================================

' Directory to copy
sourceDir = "\\NAS\Directory-1"

' Destination of copy job
destinationDir = "\\NAS\Directory-2"

' ====================================

' Main Script

Set fso = CreateObject("Scripting.FileSystemObject")
Set objShell = CreateObject("Shell.Application")

const OverwriteExisting = True
strDirectory = destinationDir & "\" & replace(date,"/","_")

if not fso.FolderExists(strDirectory) then
   Set objFolder = fso.CreateFolder(strDirectory)
   
end if

fso.CopyFolder sourceDir, strDirectory & "\", OverwriteExisting

' ====================================

' Complete Message Box
MsgBox "Backup Complete", vbOKOnly, "Auto Backup"

' ====================================
